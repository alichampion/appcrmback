<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Let's truncate our existing records to start from scratch.
        Categories::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few Categoriess in our database:
        for ($i = 0; $i < 50; $i++) {
            Categories::create([
                'name' => $faker->sentence,
                'main_id' => $faker->paragraph,
                'main_id' => $faker->paragraph,
            ]);
        }
    }
}
