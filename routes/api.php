<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('accounts', 'api\Accounts');
Route::resource('requests', 'api\Requests');
Route::post( 'change_request_seen', 'api\Requests@change_request_seen');
// Route::middleware('optimizeImages')->group(function () {
    Route::post('upload_image', 'api\Common@imageUpload'); 
// });
Route::post('get-all-counts', 'api\Common@getAllCounts'); 
Route::post('categories', 'api\Categories@index');
Route::post('all-requests', 'api\Requests@get_all_requests');
Route::post('all-instructions', 'api\InstructionController@get_all_instrcutions');
Route::post('all-news', 'api\InstructionController@get_all_news');
Route::post( '/sub_categories', 'api\Categories@sub_categories');
Route::post( '/get_contractors', 'api\Accounts@get_contractors');
Route::post( '/convertToSpanish', 'api\Accounts@convertToSpanish');
Route::post( '/get_apartments', 'api\InstructionController@get_apartments');
Route::post( '/save_instructions', 'api\InstructionController@save_instructions');
Route::post( '/save_news', 'api\InstructionController@save_news');
Route::post( '/get_accounts', 'api\Accounts@get_accounts');
Route::post( '/update_accounts', 'api\Accounts@update_accounts');
Route::post( '/delete_account', 'api\Accounts@delete_account');
Route::post( '/getCounts', 'api\Requests@getCounts');
Route::post( '/change_request_status', 'api\Requests@change_request_status');
Route::post('login','api\Accounts@login');

//Notifier Route
Route::resource('notifiers', 'api\Notifiers');

Route::get('notifiers/c/{type}', 'api\Notifiers@getByType');
// Route::middleware('auth:account_api')->group(function () {

// });
