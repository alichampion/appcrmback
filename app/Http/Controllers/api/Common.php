<?php

namespace App\Http\Controllers\api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Image;
class Common extends Controller
{
    function imageUpload(Request $request){
        if ($images = $request->file('images')) {
            $image = $request->file('images')->getClientOriginalName();
            $extension = $request->file('images')->getClientOriginalExtension();
            $image = $request->prefix. '_' . date('YmdHis').str_random(3). '.' . $extension;
            // read image from temporary file
            $img = Image::make($_FILES['images']['tmp_name']);
            $img->orientate();
            $height = $img->height();$width = $img->width();
            $height_n = round($height/100*20);$width_n = round($width/100*20,0);
            // resize image
            $img->resize($width_n, $height_n);
            // save image
            $img->save(public_path().'/uploads/'.$image);
            $images_['image'] = $image;
            $images_['key'] = $request->key ? $request->key : 0;
            $images_['height']=$height;
            $images_['width']=$width;
            $images_['height_n']=$height_n;
            $images_['width_n']=$width_n;
        }
        if (isset($images_)) {
            $status = true;
            $message = "File Uploaded Successfully";
            return response()->json(['data' => $images_, 'status' => $status, 'message' => $message], 200);
        } else {
            $status = false;
            $message = "Failed To Upload File Successfully";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }
    function getAllCounts(Request $request){
        $authType=$request->authType;
        $api_token=$request->api_token;
        $countType=$request->countType;
        if($countType == 'Accounts'){
            // DB::enableQueryLog();
            $data = DB::table('news')->select('n_ref');
            if($authType == 'Client'){
                $data=$data->where('n_ref', $api_token);
            }
            $data=$data->count();
        }
        if($countType == 'Front Desk' || $countType == 'Deliveries'){
            $data = DB::table('instructions');
            if($authType == 'Client' || $authType == 'Contractor'){
                $data=$data->where('i_ref', $api_token);
            }
            $data=$data->where('i_type', "Instructions")->where('i_s_type', $countType);
            $data=$data->count();
        }
        if(isset($data) && !empty($data)){
            $status = true;
            $message = "Counts Found Successfully";
            return response()->json(['data' => $data, 'status' => $status, 'message' => $message], 200);
        }else{
            $status = false;
            $message = "No Data Found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }
}
