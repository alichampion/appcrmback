<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Notifier;

use Validator;
class Notifiers extends Controller
{
    public function index()
    {
        return Notifier::all();
    }
    public function getByType(String $type){
        return Notifier::where('type','=',$type)->get();;
    }

    public function show(Notifier $Notifier)
    {
        return $Notifier;
    }

    public function store(Request $request)
    {
        $Notifier = Notifier::create($request->all());

        return response()->json($Notifier, 201);
    }

    public function update(Request $request, Notifier $Notifier)
    {
        $Notifier->update($request->all());

        return response()->json($Notifier, 200);
    }
   
    public function destroy(Notifier $Notifier)
    {
         $Notifier->delete();

         return response()->json(null, 204);
    }

}
