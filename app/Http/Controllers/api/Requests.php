<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
class Requests extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'referred_by' => 'required',
            'main_category' => 'required',
            'sub_category' => 'required',
            'message' => 'required',
            'request_type' => 'required',
            'images'=>'required',
            'contractor'=> 'required'
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $r_ref = $request->referred_by;
            $msg = $request->message;
            $m_id = $request->main_category;
            $s_id = $request->sub_category; 
            $request_type = $request->request_type;
            $r_imgs = $request->images;
            $date = date("Y-m-d H:i:s");
            $user=DB::table('accounts')->select('contacts','email','name')->where('api_token', $r_ref)->first();
            $finalImages=[];
            if(!empty($r_imgs) && is_array($r_imgs)){
                foreach($r_imgs as $key => $value){
                    $finalImages[]=$value['image'];
                }
            }
            if($user){
                $r_name = $user->name;
                $r_email = $user->email;
                $r_phn = $user->contacts;
                $r_s_type=isset($request->r_s_type) ? $request->r_s_type : null;
                $time_=explode(':',$request->time);
                if(isset($time_[0]) && isset($time_[1])){
                    $time_f=$time_[0].' : '.str_replace('Minute','Hours',$time_[1]);
                }else{
                    $time_f=null;
                }
                $insert = [
                    'r_name'=> $r_name,
                    'r_email' => $r_email,
                    'r_type' => $request_type,
                    'r_s_type' => $r_s_type,
                    'r_phn'=> $r_phn,
                    'r_msg' => $msg,
                    'r_ref' => $r_ref,
                    'm_id' => $m_id,
                    's_id' => $s_id,
                    'r_contractor'=> $request->contractor,
                    'contractor_ref'=>'0',
                    'date' => $date,
                    'f_at' => isset($request->date) ? $request->date : null,
                    'time' => isset($request->time) ? $time_f : null,
                    'r_imgs' => implode($finalImages,','),
                    'r_status'=>"Pending"
                ];
                $data = DB::table('requests')->insertGetId($insert);
                if ($data) {
                    if(!empty($request->contractor) && $request->contractor != 0){
                        $contractors = DB::table('accounts')->select('name', 'device_token','api_token')->where('a_m_id', $request->contractor)->get();   
                        foreach ($contractors as $key => $value) {
                            send_notification(
                                [
                                    'title' => 'Hello ' . $value->name,
                                    'body' => 'You Have A New ' . $request_type . ' Request From ' . $r_name,
                                    'fcm_token' => $value->device_token
                                ],
                                [
                                    'page' => 'SingleRequest',
                                    'data' => array_merge($insert,['r_id'=>$data]),
                                    'auth_type' => 'Contractor',
                                    'auth' => $value->api_token,
                                    'date' => $date
                                ]
                            );
                        }
                    }
                    $manager = DB::table('accounts')->select('name', 'device_token','api_token')->where('type', 'Manager')->get()->first();
                    $body=isset($value->name) ? 'A Client Sent A ' . $request_type . ' Request To ' . $value->name : 'A Client Have A ' . $request_type . ' Request For '.$r_s_type;
                    send_notification(
                        [
                            'title' => 'Hello ' . $manager->name,
                            'body' => $body,
                            'fcm_token' => $manager->device_token
                        ],
                        [
                            'page' => 'SingleRequest',
                            'data' => array_merge($insert,['r_id'=>$data]),
                            'auth_type' => 'Manager',
                            'auth' => $manager->api_token,
                            'date' => $date
                        ]
                    );
                    $status = true;
                    $message = $request->r_type." Enviado con éxito";
                    return response()->json(['status' => $status, 'message' => $message], 200);
                } else {
                    $status = false;
                    $message = 'Algo salió mal o un problema con la red';
                    return response()->json(['status' => $status, 'message' => $message], 200);
                }
            }else{
                $status = false;
                $message = 'Some Thing Went Wrong Logout And Try Again';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    function get_all_requests(Request $request){
        $validate =  Validator::make($request->all(), [
            'auth_key' => 'required',
            'type_key' => 'required',
            'request_type' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $auth_type= $request->auth_type;
            if(isset( $auth_type)){
                $request_type=$request->request_type;
                $type_key=$request->type_key;
                if($auth_type == 'Client'){
                    $data = DB::table('requests')->where('r_ref', $request->auth_key)->where('r_type', $request->request_type)->where('r_status','LIKE','%'.$type_key.'%');
                    if($type_key == 'In Progress'){$data=$data->orWhere('r_status','LIKE','%Answered_Rejected%');}
                    if($request_type != 'Reservation'){
                        $data=$data->join('category', 'category.c_id','=', 'requests.s_id');
                    }
                    $data=$data->orderBy('r_id','desc')->get()->toArray();
                }
                if ($auth_type == 'Contractor') {
                    $account = DB::table('accounts')->select('api_token','a_m_id')->where('api_token', $request->auth_key)->first();
                    $data = DB::table('requests')->where( 'r_contractor', $account->a_m_id)->where('r_type', $request->request_type)->where('r_status','LIKE','%'.$type_key.'%')->join('category', 'category.c_id','=', 'requests.s_id')->orderBy('r_id','desc');
                    if($type_key == 'In Progress'){$data=$data->orWhere('r_status','LIKE','%Answered_Rejected%');}
                    $data=$data->get()->toArray();
                }
                if ($auth_type == 'Manager') {
                    $data = DB::table('requests')->where('r_type', $request->request_type)->where('r_status','LIKE', '%'.$type_key.'%');
                    if($type_key == 'In Progress'){$data=$data->orWhere('r_status','LIKE','%Answered_Rejected%');}
                    if($request_type != 'Reservation'){
                        $data=$data->join('category', 'category.c_id','=', 'requests.s_id');
                    }
                    $data=$data->orderBy('r_id','desc')->get()->toArray();
                }
            }else{
                $data = DB::table('requests')->where('r_ref', $request->auth_key)->where('r_type', $request->request_type)->where('r_status','LIKE','%'.$request->type_key.'%')->join('category', 'category.c_id','=', 'requests.s_id')->orderBy('r_id','desc')->get()->toArray();
            }
            if($data){
                $status = true;
                // if($request->isDate == 'yes'){
                //     $date1=$data[count($data) - 1]->date;
                //     $date2=$data[0]->date;
                //     $dates= getDatesFromRange($date1,$date2);
                //     // $dates_new=[];
                //     // dd( $dates);
                //     foreach($dates as $key => $value){
                //         foreach($data as $key1 => $value2){
                //             echo $value2->date.'<br>';
                //             $dates_new[$value] = $value == $value2->date ? $value2 : 'no';
                //         }
                //     }
                //     echo "<pre>";
                //     print_r($dates_new);exit;
                //     $data=$dates_new;
                // }
                $message = "Solicitudes encontradas exitosamente";
                return response()->json(['data' => $data, 'status' => $status, 'message' => $message], 200);
            }else{
                $status = false;
                $message = "No se encontraron solicitudes";
                return response()->json(['data' => [], 'status' => $status, 'message' => $message], 200);
            }
        }
    }
    function getCounts(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'auth' => 'required',
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $type = $request->type;
            // echo $type;exit;
            if (isset($type)) {
                if ($type == 'Client') {
                    $requests = DB::table('requests')->select('r_status','r_type')->where('r_ref', $request->auth)->get()->toArray();
                }
                if ($type == 'Contractor') {
                    $account = DB::table('accounts')->select('api_token', 'a_m_id')->where('api_token', $request->auth)->get()->first();
                    $requests = DB::table('requests')->select('r_status','r_type','m_id','s_id')->where('r_type','Repair')->where('r_contractor', $account->a_m_id)->get()->toArray();
                }
                if ($type == 'Manager') {
                    $requests = DB::table('requests')->select('r_status','r_type')->get()->toArray();
                }
                $counts=[];
                // dd($requests);
                foreach ($requests as $key => $value) {
                    $counts[str_replace(' ','_',$value->r_type).'_'. str_replace(' ','_',explode('_',$value->r_status)[0])][]='';
                }
                foreach ($counts as $key => $value) {
                    $counts[$key] = count($value);
                }
            }
            if ($requests) {
                $status = true;
                $message = "Solicitudes encontradas exitosamente";
                return response()->json(['data' => $counts, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = "No se encontraron solicitudes";
                return response()->json(['data' => [], 'status' => $status, 'message' => $message], 200);
            }
        }
    }
    function change_request_status(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'r_status' => 'required',
            'id' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $data = DB::table('requests')->where('r_id', $request->id)->update(['r_status'=>$request->r_status, 'contractor_ref'=> $request->auth,'f_at'=>isset( $request->date_cont) ? $request->date_cont : date('Y-m-d H:i:s'),'time'=>$request->time ]);
            $request_db = DB::table('requests')->where('r_id', $request->id)->get()->first();
            $contractor = DB::table('accounts')->select('name')->where( 'api_token', $request_db->contractor_ref)->get()->first();
            $user = DB::table('accounts')->select( 'device_token', 'name','api_token')->where( 'api_token', $request_db->r_ref)->get()->first();
            if ($data) {
                if($request->r_status == 'Answered_Accepted' || $request->r_status == 'Answered_Rejected'){
                    if($request_db->r_type == 'Reservation'){
                        $type_no=$request_db->r_s_type;
                    }else{
                        $type_no=$request_db->r_type;
                    }
                    send_notification(
                        [
                            'title' => 'Hello ' . $user->name,
                            'body' => ' Your Request For '.$type_no.' Answered By Your Manager',
                            'fcm_token' => $user->device_token
                        ],
                        [
                            'page' => 'SingleRequest',
                            'data' => $request_db,
                            'auth_type' => 'Client',
                            'auth' => $user->api_token,
                            'date' => $request_db->date,
                        ]
                    );
                }
                if($request->r_status == 'In Progress'){
                    send_notification(
                        [
                            'title' => 'Hello ' . $user->name,
                            'body' => isset( $contractor->name) ? ' Your Repair Request Accepted By '.$contractor->name : ' Your Repair Request Accepted Successfully',
                            'fcm_token' => $user->device_token
                        ],
                        [
                            'page' => 'SingleRequest',
                            'data' => $request_db,
                            'auth_type' => 'Client',
                            'auth' => $user->api_token,
                            'date' => $request_db->date,
                        ]
                    );
                }
                if($request->r_status == 'Completed'){
                    $manager = DB::table('accounts')->select('name', 'device_token','api_token')->where('type', 'Manager')->get()->first();
                    send_notification(
                        [
                            'title' => 'Hello ' . $user->name,
                            'body' => isset( $contractor->name) ? 'Your Repair Request Completed By '.$contractor->name : ' Your Repair Request Completed Successfully',
                            'fcm_token' => $user->device_token
                        ],
                        [
                            'data'=> $request_db
                        ],
                        [
                            'page' => 'SingleRequest',
                            'data' => $request_db,
                            'auth_type' => 'Client',
                            'auth' => $user->api_token,
                            'date' => $request_db->date,
                        ]
                    );
                    send_notification(
                        [
                            'title' => 'Hello ' . $manager->name,
                            'body' => 'Client '.$user->name.' Request Completed By '.$contractor->name,
                            'fcm_token' => $manager->device_token
                        ],
                        [
                            'page' => 'SingleRequest',
                            'data' => $request_db,
                            'auth_type' => 'Manager',
                            'auth' => $manager->api_token,
                            'date' => $request_db->date,
                        ]
                    );
                }
                $status = true;
                $message = "Datos actualizados exitosamente";
                return response()->json(['data' => $data, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = "Datos no actualizados correctamente";
                return response()->json(['data' => [], 'status' => $status, 'message' => $message], 200);
            }
        }
    }
    function change_request_seen(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'is_seen' => 'required',
            'id' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $data = DB::table('requests')->where('r_id', $request->id)->update(['is_seen' => $request->is_seen]);
            if ($data) {
                $status = true;
                $message = "Datos actualizados exitosamente";
                return response()->json(['data' => $data, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = "Datos no actualizados correctamente";
                return response()->json(['data' => [], 'status' => $status, 'message' => $message], 200);
            }
        }
    }
}
