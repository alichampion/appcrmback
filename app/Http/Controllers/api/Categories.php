<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
class Categories extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $type=$request->type;
            if($type == "Contractor"){
                $data = DB::table('category')->where('catregory_parent_id', '0')->where('category_type', $type)->get()->toArray();
            }elseif( $type == "Category"){
                $apartment_type = isset($request->apartment_type) ? $request->apartment_type : "";
                $data = DB::table('category')->where('catregory_parent_id', '0')->where('category_type', $type)->where( 'cat_aprt_type', 'LIKE',"%{$apartment_type}%")->get()->toArray();
            }
            if (!empty($data)) {
                $res = [];
                foreach ($data as $key => $value) {
                    $res[] = ['id' => $value->c_id, 'name' => $value->category_name];
                }
                $status = true;
                $message = "Datos obtenidos con éxito";
                return response()->json(['data' => $res, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = 'Some Thing Went Wrong Or Promblem With Network';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function sub_categories(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validate->fails()){
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        }else{
            $data = DB::table('category')->where( 'catregory_parent_id', $request->id)->get()->toArray();
            if (!empty($data)) {
                $res = [];
                foreach ($data as $key => $value) {
                    $res[] = ['id' => $value->c_id, 'name' => $value->category_name, 'contractor'=>$value->cat_contractor];
                }
                $status = true;
                $message = "Datos obtenidos con éxito";
                return response()->json(['data' => $res, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = 'Some Thing Went Wrong Or Promblem With Network';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
}
