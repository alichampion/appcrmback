<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
Use Illuminate\Support\Str;
class Accounts extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=DB::table('accounts')->get();
        $data['users']= $users;
        return response()->json($data, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'email' => 'required|email|unique:accounts',
            'name' => 'required|min:3',
            'password' => 'required|min:6',
            'contacts' => 'required|min:10',
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $apartment_type=$request->apartment_type;
            $apartment = $request->apartment_no;
            $apartments = [
                '201', '202', '203', '204', '205', '206', '207', '208',
                '301', '302', '303', '304', '305', '306', '307', '308',
                '401', '402', '403', '404', '405', '406', '407', '408',
                '501', '502', '503', '504', '505', '506', '507', '508',
                '601', '602', '603', '604', '605', '606', '607', '608',
                '701', '702', '703', '704', '705', '706', '707', '708',
                '801', '802', '803', '804', '805', '806', '807', '808',
            ];
            if ($request->type !== 'Contractor' && !in_array($apartment, $apartments)) {
                $status = false;
                $message = 'El apartamento no es inválido Ingrese el apartamento válido no.';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
            if ($request->type !== 'Contractor' && $apartment_type != "A" && $apartment_type != "B" && $apartment_type != "C" && $apartment_type != "") {
                $status = false;
                $message = 'El Tipo de contratista solo puede ser A, B o C';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
            $request->request->add(['api_token' => Str::random()]);
            $data=$request->all();
            $data= DB::table('accounts')->insert($data);
            if($data){
                $status = true;
                $message = $request->type." Cuenta creada exitosamente";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }else{
                $status = false;
                $message = 'Algo salió mal o un problema con la red';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function login(Request $request)
    {
        $validate =  Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required|min:6'
        ]);
        if( $validate->fails()){
        $status = false;
        $message = $validate->errors()->first();
        return response()->json(['status' => $status, 'message' => $message], 200);
      }else{
            $email=$request->email;
            $pass=$request->password;
            $user=DB::table('accounts')->select('type','email','password','apartment_no','apartment_type','device_token','a_id', 'api_token')->where('email',$email)->where('password', $pass)->get()->toArray();
            if(isset($user[0]) && !empty($user[0])){
                $data=$user[0];
                DB::table('accounts')->where('email', $email)->where('password', $pass)->update(['device_token'=>$request->device_token,'a_up_at'=>date('d-m-Y h:i:s')]);
                $status = true;
                $message = "Welcome Back You Logged In Successfully";
                // if($data->type == 'Client'){
                //     $repair_requests = DB::table('requests')->select( 'r_ref', 'r_type', 'r_status')->where('r_ref', $data->api_token)->get();
                //     foreach ($repair_requests as $key => $value) {
                        
                //     }
                //     dd($repair_requests);
                // }else{
                    
                // }
                // dd($repair_requests);
                return response()->json(['data'=>$data,'status' => $status, 'message' => $message], 200);
            }else{
                $status = false;
                $message = "No válida de correo electrónico o la contraseña";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
      }
    }
    public function get_contractors(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'm_id' => 'required',
            's_id' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $data = DB::table('accounts')->where('type', "Contractor")->where('a_m_id', $request->m_id)->where('a_s_id', $request->s_id)->get()->toArray();
            if (!empty($data)) {
                $res = [];
                foreach ($data as $key => $value) {
                    $res[] = ['id' => $value->a_id, 'name' => $value->name,'api_token'=>$value->api_token];
                }
                $status = true;
                $message = "Datos obtenidos con éxito";
                return response()->json(['data' => $res, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = 'Algo salió mal o un problema con la red';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
    public function get_accounts(Request $request)
    {
        $data = DB::table('accounts')->where('type', '!=', "Manager")->leftJoin( 'category', 'accounts.a_m_id', '=', 'category.c_id')->get()->toArray();
        if (!empty($data)) {
            $res = [];
            foreach ($data as $key => $value) {
                $res[$value->type][] = $value;
            }
            $status = true;
            $message = "Datos obtenidos con éxito";
            return response()->json(['data' => $res, 'status' => $status, 'message' => $message], 200);
        } else {
            $status = false;
            $message = 'Algo salió mal o un problema con la red';
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }
    public function delete_account(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'id' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $data = DB::table('accounts')->where('a_id',$request->id)->delete();
            if (!empty($data)) {
                $status = true;
                $message = "Cuenta eliminada exitosamente";
                return response()->json(['status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = 'Algo salió mal o un problema con la red';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
    public function update_accounts(Request $request){
        $account=DB::table('accounts')->where('a_id',$request->a_id)->get()->first();
        $validation=[
            'a_id' => 'required',
            'name' => 'required|min:3',
            'password' => 'required|min:6',
            'contacts' => 'required|min:10',
            'type' => 'required',
        ];
        if($account->email != $request->email){
            $validation=array_merge($validation,['email' => 'required|email|unique:accounts']);
        }
        $validate =  Validator::make($request->all(), $validation);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $apartment_type = $request->apartment_type;
            $apartment = $request->apartment_no;
            $apartments = [
                '201', '202', '203', '204', '205', '206', '207', '208',
                '301', '302', '303', '304', '305', '306', '307', '308',
                '401', '402', '403', '404', '405', '406', '407', '408',
                '501', '502', '503', '504', '505', '506', '507', '508',
                '601', '602', '603', '604', '605', '606', '607', '608',
                '701', '702', '703', '704', '705', '706', '707', '708',
                '801', '802', '803', '804', '805', '806', '807', '808',
            ];
            if ($request->type !== 'Contractor' && !in_array($apartment, $apartments)) {
                $status = false;
                $message = 'El apartamento no es inválido Ingrese el número de apartamento válido';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
            if ($request->type !== 'Contractor' && $apartment_type != "A" && $apartment_type != "B" && $apartment_type != "C" && $apartment_type != "") {
                $status = false;
                $message = 'El Tipo de contratista solo puede ser A, B o C';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
            $data = $request->all();
            $data = DB::table('accounts')->where('a_id',$request->a_id)->update($data);
            $status = true;
                $message = $request->type . " Account Updated Successfully";
                return response()->json(['status' => $status, 'message' => $message], 200);
            // if ($data) {
            //     $status = true;
            //     $message = $request->type . " Account Updated Successfully";
            //     return response()->json(['status' => $status, 'message' => $message], 200);
            // } else {
            //     $status = false;
            //     $message = 'Algo salió mal o un problema con la red';
            //     return response()->json(['status' => $status, 'message' => $message], 200);
            // }
        }
    }
    public function convertToSpanish(Request $request){
        return convertToSpanish($request->text);
    }
}
