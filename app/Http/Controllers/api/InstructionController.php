<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
class InstructionController extends Controller
{
    public function get_apartments(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'apartmentType' => 'required',
            'accountType' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $data = DB::table('accounts')->where('type', $request->accountType)->where('apartment_type', $request->apartmentType)->get()->toArray();
            if (!empty($data)) {
                $status = true;
                $message = "Datos obtenidos con éxito";
                return response()->json(['data' => $data, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = 'Algo salió mal o un problema con la red';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
    
    public function save_instructions(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'instType' => 'required',
            'type'=>'required',
            'instRef'=>'required'
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $type=$request->type;
            $instType=$request->instType;
            $instRef=$request->instRef;
            $i_inst=$request->i_inst;
            $insert=[
                'i_type'=>$type,
                'i_s_type'=>$instType,
                'i_ref'=>$instRef,
                'i_inst'=>$i_inst,
                'c_at' => date('Y-m-d H:i:s')
            ];
            $data = DB::table('instructions')->insert($insert);
            if($data){
                $Client = DB::table('accounts')->select('name','device_token')->where('api_token', $instRef)->first();
                if(!empty($Client)){
                    send_notification(
                        [
                            'title' => 'Hello ' . $Client->name,
                            'body' => 'You Have A '.$instType.' '.$type.' From Your Manager',
                            'fcm_token' => $Client->device_token
                        ],
                        [
                            'page' => 'InstructionDetails',
                            'data' => $insert,
                            'auth_type' => $request->accountType,
                            'auth' => $instRef,
                        ]
                    );
                }
                $status = true;
                $message = $request->type." Enviado con éxito";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }else{
                $status = false;
                $message = 'Algo salió mal o un problema con la red';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
    
    
    public function save_news(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'type' => 'required',
            'newsType'=>'required',
            'accountType'=>'required'
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $type=$request->type;
            $newsType=$request->newsType;
            $instRef=$request->instRef;
            $newsDetails=$request->newsDetails;
            $accountType=$request->accountType;
            $data=true;
            if($data){
                $Client = DB::table('accounts')->select('name','device_token','api_token')->where('type', $accountType)->get()->toArray();
                if($newsType == 'Maintenance Amount'){
                    $ammount=isset($request->amount) ? $request->amount : 0;
                    if(!empty($Client)){
                        foreach($Client as $key => $value){
                            $insert=[
                                    'n_type'=>"News",
                                    'n_s_type'=>$newsType,
                                    'n_inst'=>$newsDetails,
                                    'n_ref'=>$value->api_token,
                                    'n_amount'=> $ammount,
                                    'c_at' => date('Y-m-d H:i:s')
                            ];
                            $data = DB::table('news')->insert($insert);
                            send_notification(
                                [
                                    'title' => 'Hello ' . $value->name,
                                    'body' => 'Tiene una nueva actualización con respecto a las cuentas de su gerente',
                                    'fcm_token' => $value->device_token
                                ],
                                [
                                    'page' => 'newsDetails',
                                    'data' => $insert,
                                    'auth_type' => $request->accountType,
                                    'auth' => $instRef,
                                ]
                            );
                        }
                    }
                }else{
                    foreach($Client as $key => $value){
                        // dd($value->api_token);
                        $insert=[
                            'i_type'=>"Instructions",
                            'i_s_type'=>"Front Desk",
                            'i_inst'=>$newsDetails,
                            'i_ref'=>$value->api_token,
                            'c_at' => date('Y-m-d H:i:s')
                        ];
                        $data = DB::table('instructions')->insert($insert);   
                        if(!empty($value->device_token)){
                                send_notification(
                                [
                                    'title' => 'Hello ' . $value->name,
                                    'body' => 'Tiene una nueva instrucción de recepción de su gerente',
                                    'fcm_token' => $value->device_token
                                ],
                                [
                                    'page' => 'InstructionDetails',
                                    'data' => $insert,
                                    'auth_type' => $request->accountType,
                                    'auth' => $instRef,
                                ]
                            );
                        }
                    }
                }
                $status = true;
                $message = $request->type." Enviado con éxito";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }else{
                $status = false;
                $message = 'Algo salió mal o un problema con la red';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
    
    function get_all_instrcutions(Request $request){
        $validate =  Validator::make($request->all(), [
            'auth_key' => 'required',
            'type_key' => 'required',
            'request_type' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $auth_type= $request->auth_type;
            if(isset( $auth_type)){
                $request_type=$request->request_type;
                $data = DB::table('instructions')->join('accounts', 'accounts.api_token','=', 'instructions.i_ref');
                if($auth_type == 'Client' || $auth_type == 'Contractor'){
                    $data=$data->where('i_ref', $request->auth_key);
                }
                $data=$data->where('i_type', $request->request_type)->where('i_s_type', $request->type_key)->orderBy('i_id','desc')->get()->toArray();
            }else{
                $data=[];
            }
            if($data){
                $status = true;
                $message = "Instrucciones encontradas con éxito";
                return response()->json(['data' => $data, 'status' => $status, 'message' => $message], 200);
            }else{
                $status = false;
                $message = "No se encontraron instrucciones";
                return response()->json(['data' => [], 'status' => $status, 'message' => $message], 200);
            }
        }
    }
    function get_all_news(Request $request){
        $validate =  Validator::make($request->all(), [
            'auth_key' => 'required',
            'type_key' => 'required',
            'request_type' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $auth_type= $request->auth_type;
            if(isset( $auth_type)){
                $request_type=$request->request_type;
                $data = DB::table('news')->join('accounts', 'accounts.api_token','=', 'news.n_ref');
                if($auth_type == 'Client' || $auth_type == 'Contractor'){
                    $data=$data->where('n_ref', $request->auth_key);
                }
                $data=$data->where('n_type', $request->request_type)->where('n_s_type', $request->type_key)->orderBy('n_id','desc')->get()->toArray();
            }else{
                $data=[];
            }
            if($data){
                $status = true;
                $message = "Noticias encontradas con éxito";
                return response()->json(['data' => $data, 'status' => $status, 'message' => $message], 200);
            }else{
                $status = false;
                $message = "No se encontraron noticias";
                return response()->json(['data' => [], 'status' => $status, 'message' => $message], 200);
            }
        }
    }
    function getCounts(Request $request)
    {
        $validate =  Validator::make($request->all(), [
            'auth' => 'required',
            'type' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $type = $request->type;
            if (isset($type)) {
                if ($type == 'Client') {
                    $requests = DB::table('requests')->select('r_status','r_type')->where('r_ref', $request->auth)->get()->toArray();
                }
                if ($type == 'Contractor') {
                    $account = DB::table('accounts')->select('api_token', 'a_m_id')->where('api_token', $request->auth)->get()->first();
                    $requests = DB::table('requests')->select('r_status','r_type','m_id','s_id')->where('r_type','Repair')->where('r_contractor', $account->a_m_id)->get()->toArray();
                }
                if ($type == 'Manager') {
                    $requests = DB::table('requests')->select('r_status','r_type')->get()->toArray();
                }
                $counts=[];
                foreach ($requests as $key => $value) {
                    $counts[str_replace(' ','_',$value->r_type).'_'. str_replace(' ','_',$value->r_status)][]='';
                }
                foreach ($counts as $key => $value) {
                    $counts[$key] = count($value);
                }
            }
            if ($requests) {
                $status = true;
                $message = "Solicitudes encontradas exitosamente";
                return response()->json(['data' => $counts, 'status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = "No se encontraron solicitudes";
                return response()->json(['data' => [], 'status' => $status, 'message' => $message], 200);
            }
        }
    }
}
