<?php
function send_notification($data, $more='')
{
    $token=$data['fcm_token'];
    $search = 'ExponentPushToken';
    if(preg_match("/{$search}/i", $token)) {
        $payload = array(
            'to' => $token,
            'sound' => 'default',
            'title' => $data['title'],
            'body' => $data['body'],
            'data' => $more,
            'priority'=>'high'
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://exp.host/--/api/v2/push/send",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($payload),
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Accept-Encoding: gzip, deflate",
            "Content-Type: application/json",
            "cache-control: no-cache",
            "host: exp.host"
        ),
      ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return "cURL Error #:" . $err;
      } else {
          return $response;
      }
  }else{
    $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
    $data = array_merge($data, ['icon' => '', 'sound' => '']);
    $notification = [
        'title' => $data['title'],
        'body' => $data['body'],
        'icon' => $data['icon'],
        'sound' => $data['sound'],
    ];
    $extraNotificationData = ["message" => $notification, "more_data" => $more];
    $fcmNotification = [
            'to' => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];
        $headers = [
            'Authorization: key=' . 'AIzaSyAQYUBYO7w5b8cTP0hQuwAYwhknQU4n44M',
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
// $fcm_token = 'c756Arg31dc:APA91bGO8FmUznVqxJD0pmvPQrZgxxEva1-N595m7PdBKV5ElNvClFb4g2nFeFvKDQZWX1F8A5JlQDB46IEvwlCRuBU1wdX2AIcB2o7sGhCqRxRDsnl898DKQcaaypwpfihsbyjCJ5ze';
// $data = ['title' => 'Hello User', 'body' => 'This is The Body of Message', 'fcm_token' => $fcm_token];
// send_notification($data);
function getDatesFromRange($start, $end, $format = 'Y-m-d')
{
    $array = array();
    $interval = new DateInterval('P1D');
    $realEnd = new DateTime($end);
    $realEnd->add($interval);
    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    foreach ($period as $date) {
        $array[] = $date->format($format);
    }    return $array;
// Call the function
// $dates = getDatesFromRange('2010-10-01', '2010-10-05');
}
function searchForSpanish($word){
    $words=[
        'Apartment'=>"Apartamento",
        'No'=>"No",
        'Is'=>"Es",
        'Invalid'=>"Inválida",
        'Please'=>"Por favor",
        'Enter'=>"Entrar",
        'Valid'=>"Válida",
        '.'=>".",
        'Type'=>"Tipo",
        'Can'=>"lata",
        'Only'=>"Sólo",
        'Be'=>"Ser",
        'Contractor'=>"Contratista",
        'User'=>"Usuario",
        'Created'=>"Creado",
        'Sent'=>"Expedido",
        'Successfully'=>"Exitosamente",
        'For'=>"por",
        'Some'=>"Algunas",
        'Thing'=>"Cosa",
        'Went'=>"Fuimos",
        'Wrong'=>"Incorrecto",
        'Or'=>"O",
        'Problem'=>"Problema",
        'With'=>"Con",
        'Network'=>"Red",
        'Welcome'=>"Bienvenidas",
        'Back'=>"atrás",
        'You'=>"Tú",
        'Login'=>"iniciar sesión",
    ];
    return isset($words[$word]) ? $words[$word] : "Not Found";
}
function convertToSpanish($text){
    $string='';
    $stArray=explode(' ',$text);
    foreach($stArray as $key => $value){
        $string=$string.' '.searchForSpanish($value);
    }
    return $string;
}
?>