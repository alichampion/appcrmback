<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifier extends Model
{
    //
    protected $fillable = ['title', 'description','images','sendTo','type'];

}
