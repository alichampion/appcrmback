-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 25, 2019 at 08:51 PM
-- Server version: 10.2.25-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aktechprojects_appcrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `a_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'User' COMMENT 'Account Valy Ki Type konsi Hai Yani Admin Yan Vendor Yan Phir User',
  `name` varchar(60) DEFAULT NULL COMMENT 'Account Valy Ka Name',
  `email` varchar(45) DEFAULT NULL COMMENT 'Account Valy Ka Email',
  `password` varchar(255) DEFAULT NULL COMMENT 'Account Valy Ka Password',
  `contacts` varchar(255) DEFAULT NULL COMMENT 'Account Valy Ky Contact No''s',
  `apartment_no` int(11) DEFAULT 1500 COMMENT 'Account Valy Ka Apartment no',
  `apartment_type` varchar(255) DEFAULT NULL,
  `a_imgs` varchar(255) DEFAULT 'user.png' COMMENT 'Account Valy Ki Image',
  `a_status` varchar(255) DEFAULT 'Approved' COMMENT 'Account Valay ka status ky wo approved hai yan uapproved',
  `a_pw_rest` varchar(11) DEFAULT NULL COMMENT 'Account Valy Ky Forgot Password ka Link',
  `a_m_id` varchar(255) DEFAULT '0',
  `a_s_id` varchar(255) DEFAULT '0',
  `a_at` timestamp NULL DEFAULT current_timestamp() COMMENT 'Account Valy Ki Signup Date',
  `api_token` varchar(255) NOT NULL,
  `device_token` text DEFAULT NULL,
  `a_reff_by` varchar(255) NOT NULL DEFAULT 'Manager' COMMENT 'This is the reference That This user is added by admin or other',
  `a_up_at` varchar(255) DEFAULT NULL COMMENT 'Account Valy Ki Proffile Update Date'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`a_id`, `type`, `name`, `email`, `password`, `contacts`, `apartment_no`, `apartment_type`, `a_imgs`, `a_status`, `a_pw_rest`, `a_m_id`, `a_s_id`, `a_at`, `api_token`, `device_token`, `a_reff_by`, `a_up_at`) VALUES
(1, 'Manager', 'Ali hussnain', 'manager@gmail.com', 'manager', '03074759377', 1500, '', 'logo.jpg', 'Approved', NULL, '0', '0', '2019-04-22 04:31:23', 'bRZTvIDVkusVJ1Bxhuhu', 'e-5mYzrMiyE:APA91bHs6PiqPKHPEIHrs7ocpukKwh4zGu1_o_qjRa0Bfd5TsKJv_dSqXmrch_Z4HukCkYTILO6A6wnmletdednj03wdyrgeD46RB4m3Gi_o4gBzmukiOqiLcO4F3-Lpddq9FfXJAETK', 'Manager', '22-06-2019 02:31:42'),
(20, 'Contractor', 'Shan Kha', 'contractor@gmail.com', 'contractor', '0303399389', 1500, 'A', 'user.png', 'Approved', NULL, '1', '0', '2019-05-18 15:13:05', 'dttVAfFvaX9UBSVn', 'dNzYRafeD6A:APA91bFPXO3-2_xkQnN0-0ToGjFP6-UEBPKMO1i9yCJsdOyRVxso-bdvgUPRIoalJomEm1HF03RLeX7T6RtpVOd-d8bvhpwpU6iAGqburIisUQgrnCIFrwCuFdRGmG97nXxZw2dsKWmx', 'Manager', '22-06-2019 01:52:33'),
(22, 'Contractor', 'Carpentry contractor', 'carpentrycontractor@gmail.com', 'Carpentrycontractor', '030585869676', NULL, 'A', 'user.png', 'Approved', NULL, '1', '0', '2019-05-29 10:25:46', 'La7thoqwEnXdy7qb', NULL, 'Manager', NULL),
(25, 'Contractor', 'Electric contractor', 'electricalcontractor@gmail.com', 'Electriccontractor', '0304546986899', NULL, 'A', 'user.png', 'Approved', NULL, '4', '0', '2019-05-29 10:29:28', 'pWByYXUUCaLDVLIu', NULL, 'Manager', NULL),
(26, 'Contractor', 'Grival contractor', 'grivalcontractor@gmail.com', 'Grivalcontractor', '0348758497679', NULL, 'A', 'user.png', 'Approved', NULL, '5', '0', '2019-05-29 10:31:38', 'QHBS5Vy6kFx7hJGP', NULL, 'Manager', NULL),
(27, 'Contractor', 'Sanitary contractor', 'sanitarycontractor@gmail.com', 'Sanitarycontractor', '038579799797', NULL, 'A', 'user.png', 'Approved', NULL, '6', '0', '2019-05-29 10:33:11', 'kwVBVNtvMm4UZRJB', NULL, 'Manager', NULL),
(28, 'Contractor', 'Drywall contractor', 'drywallcontractor@gmail.com', 'Drywallcontractor', '03849799797', NULL, 'A', 'user.png', 'Approved', NULL, '7', '0', '2019-05-29 10:35:28', '3LjFEfKalKBJBUSm', NULL, 'Manager', NULL),
(29, 'Contractor', 'Glass contractor', 'glasscontractor@gmail.com', 'Glasscontractor', '039767978979', NULL, 'A', 'user.png', 'Approved', NULL, '8', '0', '2019-05-29 10:38:13', 'Hf1IT1lHQMsK4yaB', NULL, 'Manager', NULL),
(32, 'Client', 'Ali Hussnain', 'user@gmail.com', 'user123', '076799797960', 506, 'A', 'user.png', 'Approved', NULL, '0', '0', '2019-06-02 14:52:32', 'XPomHz5zHwcbXS9U', 'fS2YReqY_XU:APA91bGO1rQJKiM8-Ol2sX0PBUCDCLgS___qLIxleCPa0ClNK9Zb3lc59SKJUGfEtMV5bVh5Q1z1nMgODICNQfXotdo-dtQH28rM2bhz-f4awk7MI4lFAFkxiT7s7sqMgUMk-FOQL30g', 'Manager', '23-06-2019 02:13:46'),
(34, 'Contractor', 'Hahshxhxush', 'jahshs7@gmail.com', 'bzuzhzbz', '6497679797976', NULL, 'A', 'user.png', 'Approved', NULL, '3', '0', '2019-06-22 13:50:40', 'PdVJYumS5MARdbF9', NULL, 'Manager', NULL),
(35, 'Client', 'Sjshzhzbz', 'sgdhsh@gmail.com', 'NhgcwfxP', '0349897675497', 507, 'A', 'user.png', 'Approved', NULL, '0', '0', '2019-06-22 14:25:45', 'gHR7su3etBphmBtQ', NULL, 'Manager', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `c_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `catregory_parent_id` int(11) NOT NULL DEFAULT 0,
  `category_type` varchar(50) NOT NULL DEFAULT 'Category',
  `cat_aprt_type` varchar(255) DEFAULT NULL,
  `cat_contractor` int(11) DEFAULT NULL,
  `category_image` text NOT NULL,
  `c_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `category_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`c_id`, `category_name`, `catregory_parent_id`, `category_type`, `cat_aprt_type`, `cat_contractor`, `category_image`, `c_at`, `category_order`) VALUES
(1, 'Contratista De Carpinteria', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:07:44', 0),
(2, 'Contratista General', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:08:13', 0),
(3, 'Contratista De Pintura', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:08:13', 0),
(4, 'Contratista Electrico', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:09:33', 0),
(5, 'Contratista Grival', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:10:26', 0),
(6, 'Contratista Sanitario E Hidraulico', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:10:26', 0),
(7, 'Contratista De Drywall', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:11:07', 0),
(8, 'Contratista De Vidrios', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:11:43', 0),
(9, 'Contratista Electrodomesticos', 0, 'Contractor', NULL, NULL, '', '2019-05-11 12:12:14', 0),
(28, 'Alcoba Principal  ', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(29, 'Alcove 1', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(30, 'Alcove 2', 0, 'Category', 'A', NULL, '', '2019-05-16 18:18:59', 0),
(31, 'Baño alcoba principal', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(32, 'Baño auxiliar', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(33, 'Hall', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(34, 'Cocina', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(35, 'Lavanderia', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(36, 'Sala', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(37, 'Balcon', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(38, 'Comedor', 0, 'Category', 'A,B,C', NULL, '', '2019-05-16 18:18:59', 0),
(39, 'Estudio', 0, 'Category', 'A', NULL, '', '2019-05-16 18:18:59', 0),
(40, 'Accesorios Muebles (Closet)', 28, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(41, 'Accesorios Muebles (Closet)', 29, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(42, 'Accesorios Muebles (Closet)', 30, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(43, 'Accesorios Muebles (Closet)', 31, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(44, 'Accesorios Muebles (Closet)', 32, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(45, 'Accesorios Muebles (Closet)', 34, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(46, 'Accesorios Muebles (Closet)', 35, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(47, 'Enchape (Por mala instalacion)', 28, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(48, 'Muros por acentamiento', 28, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(49, 'Muros por acentamiento', 29, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(50, 'Muros por acentamiento', 30, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(51, 'Pintura por acentamiento', 28, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(52, 'Pintura por acentamiento', 29, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(53, 'Pintura por acentamiento', 30, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(54, 'Pintura por acentamiento', 31, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(55, 'Pintura por acentamiento', 32, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(56, 'Pintura por acentamiento', 33, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(57, 'Pintura por acentamiento', 34, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(58, 'Pintura por acentamiento', 35, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(59, 'Pintura por acentamiento', 36, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(60, 'Pintura por acentamiento', 37, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(61, 'Pintura por acentamiento', 38, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(62, 'Pintura por acentamiento', 39, 'Category', NULL, 3, '', '2019-05-11 12:39:23', 0),
(63, 'Accesorios ventanas ', 28, 'Category', NULL, 8, '', '2019-05-11 12:43:58', 0),
(64, 'Accesorios ventanas ', 29, 'Category', NULL, 8, '', '2019-05-11 12:43:58', 0),
(65, 'Accesorios ventanas ', 30, 'Category', NULL, 8, '', '2019-05-11 12:43:58', 0),
(66, 'Accesorios ventanas ', 36, 'Category', NULL, 8, '', '2019-05-11 12:43:58', 0),
(67, 'Instalacion Electrica', 28, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(68, 'Instalacion Electrica', 29, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(69, 'Instalacion Electrica', 30, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(70, 'Instalacion Electrica', 31, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(71, 'Instalacion Electrica', 32, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(72, 'Instalacion Electrica', 33, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(73, 'Instalacion Electrica', 35, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(74, 'Drywall', 28, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(75, 'Drywall', 29, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(76, 'Drywall', 30, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(77, 'Drywall', 31, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(78, 'Drywall', 32, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(79, 'Drywall', 33, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(80, 'Drywall', 34, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(81, 'Drywall', 35, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(82, 'Drywall', 36, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(83, 'Drywall', 37, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(84, 'Drywall', 38, 'Category', NULL, 7, '', '2019-05-11 12:40:34', 0),
(85, 'Switches', 28, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(86, 'Switches', 29, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(87, 'Switches', 30, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(88, 'Switches', 31, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(89, 'Switches', 32, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(90, 'Switches', 33, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(91, 'Switches', 34, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(92, 'Switches', 35, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(93, 'Switches', 36, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(94, 'Switches', 39, 'Category', NULL, 4, '', '2019-05-11 12:40:34', 0),
(95, 'Manijas', 28, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(96, 'Manijas', 29, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(97, 'Manijas', 30, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(98, 'Manijas', 31, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(99, 'Manijas', 32, 'Category', NULL, 1, '', '2019-05-11 12:14:14', 0),
(100, 'Muros por acentamiento', 31, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(101, 'Muros por acentamiento', 32, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(102, 'Muros por acentamiento', 33, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(103, 'Muros por acentamiento', 34, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(104, 'Muros por acentamiento', 35, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(105, 'Muros por acentamiento', 36, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(106, 'Muros por acentamiento', 37, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(107, 'Muros por acentamiento', 38, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(108, 'Muros por acentamiento', 39, 'Category', NULL, 2, '', '2019-05-11 12:14:14', 0),
(109, 'Griferias', 31, 'Category', NULL, 5, '', '2019-05-11 12:40:34', 0),
(110, 'Griferias', 32, 'Category', NULL, 5, '', '2019-05-11 12:40:34', 0),
(111, 'Griferias', 34, 'Category', NULL, 5, '', '2019-05-11 12:40:34', 0),
(112, 'Instalacion hidraulica y sanitaria', 31, 'Category', NULL, 6, '', '2019-05-11 12:40:34', 0),
(113, 'Instalacion hidraulica y sanitaria', 32, 'Category', NULL, 6, '', '2019-05-11 12:40:34', 0),
(114, 'Accesorios puerta corredera', 37, 'Category', NULL, 8, '', '2019-05-11 12:43:58', 0),
(115, 'Horno (Del Proveedor)', 34, 'Category', NULL, 9, '', '2019-05-11 12:45:08', 0),
(116, 'Estufa (Del proveedor)', 34, 'Category', NULL, 9, '', '2019-05-11 12:45:08', 0),
(117, 'Campana ( Del proveedor)', 34, 'Category', NULL, 9, '', '2019-05-11 12:45:29', 0),
(118, 'Calentador (Del proveedor)', 34, 'Category', NULL, 9, '', '2019-05-11 12:45:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `r_id` int(11) NOT NULL,
  `r_type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `r_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `r_email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `r_phn` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `r_msg` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `r_imgs` mediumtext COLLATE utf8mb4_bin DEFAULT NULL,
  `r_ref` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `r_contractor` int(11) NOT NULL,
  `m_id` int(11) DEFAULT 0,
  `s_id` int(11) DEFAULT 0,
  `contractor_ref` varchar(255) COLLATE utf8mb4_bin DEFAULT '0',
  `date` date DEFAULT NULL,
  `f_at` timestamp NULL DEFAULT NULL COMMENT 'Finished At',
  `r_status` varchar(15) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Pending',
  `is_seen` int(11) NOT NULL DEFAULT 0,
  `r_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`r_id`, `r_type`, `r_name`, `r_email`, `r_phn`, `r_msg`, `r_imgs`, `r_ref`, `r_contractor`, `m_id`, `s_id`, `contractor_ref`, `date`, `f_at`, `r_status`, `is_seen`, `r_at`) VALUES
(1, 'Repair', 'Ali hussnain', 'user@gmail.com', '0767997979', 'Hi my bed room', 'Repair_201906150614548ni.jpg,Repair_201906150614542kO.jpg', 'XPomHz5zHwcbXS9U', 1, 29, 41, 'dttVAfFvaX9UBSVn', '2019-06-15', '2019-06-21 08:24:08', 'Completed', 1, '2019-06-15 06:14:57'),
(2, 'Repair', 'Ali hussnain', 'user@gmail.com', '0767997979', 'Hey I am Ali', 'Repair_20190615061630Jt0.jpg', 'XPomHz5zHwcbXS9U', 1, 28, 40, 'dttVAfFvaX9UBSVn', '2019-06-15', '2019-06-26 19:00:00', 'In Progress', 1, '2019-06-15 06:16:32'),
(3, 'Repair', 'Ali hussnain', 'user1@gmail.com', '0767997979', 'Fjfmcj', 'Repair_20190615062924otG.jpg', 'XPomHz5zHwcbXS9U', 1, 29, 41, 'dttVAfFvaX9UBSVn', '2019-06-15', '2019-06-21 08:25:10', 'Completed', 1, '2019-06-15 06:29:53'),
(4, 'Repair', 'Ali hussnain', 'user1@gmail.com', '0767997979', 'Bdmsbsmsnd', 'Repair_20190615063201Luc.jpg,Repair_20190615063213S3C.jpg', 'XPomHz5zHwcbXS9U', 2, 29, 49, '0', '2019-06-15', NULL, 'Pending', 0, '2019-06-15 06:32:24'),
(5, 'Repair', 'Ali hussnain', 'user1@gmail.com', '07679979790', 'Hxkdbdmd', 'Repair_20190621132755MgW.jpg', 'XPomHz5zHwcbXS9U', 1, 28, 40, 'dttVAfFvaX9UBSVn', '2019-06-21', '2019-06-20 19:00:00', 'In Progress', 1, '2019-06-21 13:27:59'),
(6, 'Repair', 'Ali hussnain', 'user@gmail.com', '07679979796', 'Chcunjvy jvg hvycybuctvugch ych hvh', 'Repair_20190621221650qr5.jpg', 'XPomHz5zHwcbXS9U', 2, 29, 49, '0', '2019-06-21', NULL, 'Pending', 0, '2019-06-21 22:17:25'),
(7, 'Repair', 'Ali hussnain', 'user@gmail.com', '07679979796', 'Sjzn zjz zjsnsis sbsbz', 'Repair_20190621224312o6B.jpg', 'XPomHz5zHwcbXS9U', 2, 30, 50, 'XPomHz5zHwcbXS9U', '2019-06-21', '2019-06-22 01:53:50', 'Completed', 0, '2019-06-21 22:43:26'),
(8, 'Repair', 'Ali hussnain', 'user@gmail.com', '07679979796', 'Thuhr', 'Repair_201906220921545vu.jpg', 'XPomHz5zHwcbXS9U', 3, 29, 52, '0', '2019-06-22', NULL, 'Pending', 0, '2019-06-22 09:21:55'),
(9, 'Repair', 'Ali hussnain', 'user@gmail.com', '07679979796', 'Suzhbdx', 'Repair_20190622104442N0l.jpg', 'XPomHz5zHwcbXS9U', 3, 29, 52, '0', '2019-06-22', NULL, 'Pending', 0, '2019-06-22 10:44:56'),
(10, 'Repair', 'Ali hussnain', 'user@gmail.com', '07679979796', 'Svsvdjxbx', 'Repair_20190622104707Bf6.jpg,Repair_20190622104708PM4.jpg', 'XPomHz5zHwcbXS9U', 3, 29, 52, '0', '2019-06-22', NULL, 'Pending', 0, '2019-06-22 10:48:03'),
(11, 'Repair', 'Ali hussnain', 'user@gmail.com', '07679979796', 'Yahshs', 'Repair_20190622112741Md7.jpg,Repair_20190622112747EAy.jpg,Repair_20190622112752hlg.jpg', 'XPomHz5zHwcbXS9U', 4, 30, 87, '0', '2019-06-22', NULL, 'Pending', 0, '2019-06-22 11:28:32'),
(12, 'Repair', 'Ali Hussnain', 'user@gmail.com', '076799797960', 'Problem', 'Repair_20190622143313DG4.jpg,Repair_20190622143314KzW.jpg', 'XPomHz5zHwcbXS9U', 1, 29, 40, 'XPomHz5zHwcbXS9U', '2019-06-22', '2019-06-22 09:34:00', 'Completed', 0, '2019-06-22 14:33:25'),
(13, 'Repair', 'Ali Hussnain', 'user@gmail.com', '076799797960', 'Chcncv', 'Repair_20190622144608RuM.jpg,Repair_20190622144609M19.jpg', 'XPomHz5zHwcbXS9U', 2, 28, 47, '0', '2019-06-22', NULL, 'Pending', 0, '2019-06-22 14:46:23'),
(14, 'Repair', 'Ali Hussnain', 'user@gmail.com', '076799797960', 'Vjbhhk', 'Repair_20190625065139AT3.jpg,Repair_201906250651408X8.jpg', 'XPomHz5zHwcbXS9U', 2, 28, 48, '0', '2019-06-25', NULL, 'Pending', 0, '2019-06-25 06:51:44');

-- --------------------------------------------------------

--
-- Table structure for table `smtp_config`
--

CREATE TABLE `smtp_config` (
  `s_id` int(11) NOT NULL,
  `protocol` varchar(255) NOT NULL,
  `smtp_host` varchar(255) NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_pass` varchar(255) NOT NULL,
  `smtp_port` varchar(255) NOT NULL,
  `mailtype` varchar(255) NOT NULL,
  `layout` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smtp_config`
--

INSERT INTO `smtp_config` (`s_id`, `protocol`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `mailtype`, `layout`) VALUES
(1, 'smtp', 'mail.sale4trade.com', 'noreply@sale4trade.com', 'Lahore@123', '587', 'html', '                              <pre><!DOCTYPE html><html><head><style type=\"text/css\">@media screen{@font-face{font-family:\'Open Sans\';font-style:normal;font-weight:400;src:local(\'Open Sans\'), local(\'OpenSans\'), url(\'https://fonts.gstatic.com/s/opensans/v10/cJZKeOuBrn4kERxqtaUH3bO3LdcAZYWl9Si6vvxL-qU.woff\') format(\'woff\')}*{font-family:\'Open Sans\'}}</style></head><body><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse;line-height:24px;margin:0;padding:0;width:100%;font-size:17px;color:#373737;background:#f9f9f9\" width=\"100%\"><tbody><tr><td style=\"font-family:\'Open Sans\', sans-serif!important;border-collapse:collapse\" valign=\"top\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\" width=\"100%\"><tbody><tr><td style=\"font-family:\'Open Sans\', sans-serif!important;border-collapse:collapse;padding:20px 16px 12px\" valign=\"bottom\"><div style=\"text-align:center\"> <a href=\"http://sale4trade.com\" style=\"color:#439fe0;font-weight:bold;text-decoration:none;word-break:break-word\" target=\"_blank\"> <img src=\"http://sale4trade.com/assets/pb/img/company/logo.png\" style=\"outline:none;text-decoration:none;border:none;width: auto;height: 50px;\"> </a></div></td></tr></tbody></table></td></tr><tr><td style=\"font-family:\'Open Sans\', sans-serif!important;border-collapse:collapse\" valign=\"top\"><table align=\"center\" border=\"0\" cellpadding=\"32\" cellspacing=\"0\" style=\"border-collapse:collapse;background:white;border-radius:0.5rem;margin-bottom:1rem\"><tbody><tr><td style=\"text-align: center; border-collapse: collapse;\" valign=\"top\" width=\"546\"><p><b><span style=\"font-size: 24px;\">Hy {username}</span></b></p><p>{ title here }</p><p style=\"text-align: left; \">{ your message to user }</p></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"32\" cellspacing=\"0\" style=\"border-collapse:collapse;background:white;border-radius:0.5rem;margin-bottom:1rem\"><tbody><tr><td style=\"font-family:\'Open Sans\', sans-serif!important;border-collapse:collapse\" valign=\"top\" width=\"546\"><p>Some Useful Links</p><ul style=\"list-style: none;\"><li style=\"float: left;\"><a href=\"http://sale4trade.com/contact-us\" style=\"color: #252525;text-decoration: none;\"> Contact Us </a></li><li style=\"float: left;\"><a href=\"http://sale4trade.com/about-us\" style=\"color: #252525;text-decoration: none;\">| About Us </a></li><li style=\"float: left;\"><a href=\"http://sale4trade.com/blogs\" style=\"color: #252525;text-decoration: none;\">| Blog </a></li><li style=\"float: left;\"><a href=\"http://sale4trade.com/\" style=\"color: #252525;text-decoration: none;\">| Login Now </a></li><li style=\"float: left;\"><a href=\"http://sale4trade.com/\" style=\"color: #252525;text-decoration: none;\">| Register Now </a></li></ul></td></tr></tbody></table></td></tr><tr><td></td></tr></tbody></table></body></html>                          ');

-- --------------------------------------------------------

--
-- Table structure for table `web_owner_info`
--

CREATE TABLE `web_owner_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `contact` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `g_plus` varchar(255) NOT NULL,
  `viemo` varchar(255) NOT NULL,
  `linkden` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `open_time_to` varchar(255) NOT NULL,
  `open_time_from` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_owner_info`
--

INSERT INTO `web_owner_info` (`id`, `name`, `logo`, `details`, `contact`, `phone`, `email`, `adress`, `facebook`, `instagram`, `twitter`, `g_plus`, `viemo`, `linkden`, `date`, `open_time_to`, `open_time_from`) VALUES
(1, 'App Crm', 'logo.png', 'Company Description', '03330910000', '', 'info@sale4trade.com', '2 york avenue,walsall,WS29XA ENGLAND', 'https://www.facebook.com/sale4trade/', 'https://www.instagram.com/', 'https://www.twitter.com/', 'https://www.google.com/', 'https://www.youtube.com/', 'https://www.linkdein.com/', '2018-07-10', '10', '12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `smtp_config`
--
ALTER TABLE `smtp_config`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `web_owner_info`
--
ALTER TABLE `web_owner_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `smtp_config`
--
ALTER TABLE `smtp_config`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_owner_info`
--
ALTER TABLE `web_owner_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
